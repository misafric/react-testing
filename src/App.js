import Greeting from './components/Greeting';
import Async from './components/Async';
import './App.css';

function App() {
  return (
    <div>
      <Greeting />
      <Async />
    </div>
  );
}

export default App;
